@component('layouts.app')
    @section('title','Manage Product')
@section('heading')
    <div class="d-flex align-items-baseline flex-wrap mr-5">
        <!--begin::Page Title-->
        <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">Setting</h2>
        <!--end::Page Title-->
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
            <li class="breadcrumb-item text-muted">
                <a href="#" class="text-muted">General</a>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{route('products.index')}}" class="text-muted">Manage Product</a>
            </li>
        </ul>
        <!--end::Breadcrumb-->
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-custom">
                <div class="card-header">
                    <div class="card-title">
    			<span class="card-icon">
    				<i class="flaticon2-user-1 text-primary"></i>
    			</span>
                        <h3 class="card-label">Manage Product</h3>
                    </div>
                    <div class="card-toolbar">
                        <!--begin::Button-->
                        <a href="{{route('products.create')}}" class="btn btn-primary font-weight-bolder">
    				<span class="svg-icon svg-icon-md">
    					<!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
    					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
    							<rect x="0" y="0" width="24" height="24" />
    							<circle fill="#000000" cx="9" cy="15" r="6" />
    							<path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
    						</g>
    					</svg>
                        <!--end::Svg Icon-->
    				</span>New Record</a>
                        <!--end::Button-->
                    </div>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" id="kt_datatable_products" style="margin-top: 13px !important">
                            <thead>
                            <tr>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Qty</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('app/dist/products.js')}}" type="text/javascript"></script>
@endsection
@endcomponent
