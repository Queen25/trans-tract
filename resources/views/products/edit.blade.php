@component('layouts.app')
    @section('title','Manage Products')
@section('heading')
    <div class="d-flex align-items-baseline flex-wrap mr-5">
        <!--begin::Page Title-->
        <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">Setting</h2>
        <!--end::Page Title-->
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
            <li class="breadcrumb-item text-muted">
                <a href="#" class="text-muted">General</a>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{route('products.index')}}" class="text-muted">Manager Products</a>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="#" class="text-muted">Edit Products</a>
            </li>
        </ul>
        <!--end::Breadcrumb-->
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Card-->
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Edit Products</h3>
                </div>
                <!--begin::Form-->
                <form class="form" autocomplete="off" onsubmit="updateData(this,event)" method="POST">
                    <div class="card-body">
                        <div class="mb-15">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Code</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="code" value="{{$data->data->code}}" />
                                    <input type="hidden" class="form-control" id="id" value="{{$data->data->id}}" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Name</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="name" value="{{$data->data->name}}" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Qty</label>
                                <div class="col-lg-6">
                                    <input type="text" disabled class="form-control" id="qty" value="{{$data->data->qty}}" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-12 text-lg-right">
                                <button type="submit" id="saveBtn" class="btn btn-success mr-2">Update</button>
                                <a href="{{route('products.index')}}" class="btn btn-secondary">Cancel</a>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Card-->
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('app/dist/products.js')}}" type="text/javascript"></script>
@endsection
@endcomponent
