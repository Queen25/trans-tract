@section('content')
@foreach($product as $product)
    <tr id="product_{{ $product->id }}" class="product-list" data-id="{{ $product->id }}">
        <td>{{ $product->name }}</td>
        <td>
            <input type="text" data-subtotal="subtotal_{{ $product->id }}" data-product="{{ $product->id }}" name="quantity" value="0" class="form-control payments qty_{{ $product->id }} quantity" value="1" required="required">
        </td>
        <input type="hidden" name="minqty" value="{{ $product->qty }}" name="minqty" class="form-control minqty{{ $product->id }} minimun_qty">
        <td><a href="#" class="btn btn-xs btn-info" onclick="removeData(this, event)" data-product="{{ $product->id }}">X</a></td>
    </tr>
@endforeach
@stop
