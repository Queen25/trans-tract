@section('content')
    <div class="kt-portlet__body col-md-12">
        <div class="table-responsive">
            <table class="table table-striped- table-bordered table-hover product-po" id="kt_table_add_adjustment">
                <thead>
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Qty</th>
                </tr>
                </thead>
                <tbody>
                @foreach($product as $product)
                    <tr id="product_{{$product->id}}">
                        <td>
                            <input type="checkbox" data-id="{{ $product->id }}" class="checkboxName" value="#product_{{$product->id}}" data-required="true"><i></i>
                        </td>
                        <td>{{$product->name}}</td>
                        <td>{{$product->qty}}</td>
                    </tr>
                @endforeach
                </tbody>
        </div>
    </div>
@stop
