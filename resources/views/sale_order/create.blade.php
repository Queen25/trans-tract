@component('layouts.app')
    @section('title','Transaction Sale Order')
@section('heading')
    <div class="d-flex align-items-baseline flex-wrap mr-5">
        <!--begin::Page Title-->
        <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">Transaction</h2>
        <!--end::Page Title-->
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
            <li class="breadcrumb-item text-muted">
                <a href="#" class="text-muted">Transaction</a>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{route('sales_orders.index')}}" class="text-muted">Transaction Sale Order</a>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="#" class="text-muted">Create Order</a>
            </li>
        </ul>
        <!--end::Breadcrumb-->
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Card-->
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Create Order</h3>
                </div>
                <!--begin::Form-->
                <form class="form" autocomplete="off" onsubmit="createSellout(this,event)" method="POST">
                    <div class="card-body">
                        <div class="mb-15">
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3 col-sm-12">Find Vehicle</label>
                                <div class="col-lg-4 col-md-12 col-sm-12 input-group">
                                    <input type="text"
                                           class="form-control" id="vehicle" name="vehicle">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" id="BtnSave" onclick="findVehicle(event)"
                                                type="button">Find!
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Vehicle</label>
                                <div class="col-lg-4">
                                    <input type="text" disabled class="form-control" id="name" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="kt-portlet__body col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th colspan="3" style="text-align: center;">TABLE PRODUCT</th>
                                </tr>
                                <tr>
                                    <th>Name Product</th>
                                    <th style="width: 100px">Qty</th>
                                    <th style="width: 10px">Action</th>
                                </tr>
                                </thead>
                                <tbody id="productItems"></tbody>
                                <tr>
                                    <td colspan="3">
                                        <a href="#" id="btnProduct" onclick="showAddProduct(this, event)"
                                           class="btn btn-success mr-2">
                                            <i class="la la-plus"></i>
                                            Add Product
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-12 text-lg-right">
                                <button type="submit" id="saveBtn" class="btn btn-success mr-2">Save</button>
                                <a href="{{route('sales_orders.index')}}" class="btn btn-secondary">Cancel</a>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Card-->
        </div>
    </div>

<div class="modal fade bs-modal-lg" id="large" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title float-left">Product</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body data-detail">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="getCheckedBoxesSellout('checkboxName')">Add</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script src="{{asset('app/dist/sale_order.js')}}" type="text/javascript"></script>
@endsection
@endcomponent
