const mix = require('laravel-mix');

let source = 'public/app/src/';
let build = 'public/app/dist/';

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('public/app/app.js',build)
    .js('public/app/api.js',build)
    .js('public/app/helpers.js',build)
    .js(source + 'products.js', build)
    .js(source + 'vehicles.js', build)
    .js(source + 'sale_order.js', build)
    .version()


mix.disableNotifications()
