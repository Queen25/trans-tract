<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('app.logout');
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::middleware(['auth'])->group(function (){
    Route::get('products',[App\Http\Controllers\Cms\ProductController::class,'index']);
    Route::resource('products',App\Http\Controllers\Cms\ProductController::class);
    Route::post('updateProducts',[App\Http\Controllers\Cms\ProductController::class,'update']);

    Route::resource('sales_orders',\App\Http\Controllers\Cms\SaleOrderController::class);
    Route::get('/findVehicle',[\App\Http\Controllers\Cms\SaleOrderController::class,'findVehicle']);
    Route::get('/showProduct',[\App\Http\Controllers\Cms\SaleOrderController::class,'showProduct']);
    Route::get('/addProduct',[\App\Http\Controllers\Cms\SaleOrderController::class,'addProduct']);


    Route::resource('vehicles',App\Http\Controllers\Cms\VehicleController::class);
    Route::post('updateVehicles',[App\Http\Controllers\Cms\VehicleController::class,'update']);

    /*
     *  Datatable Controller
     */
    Route::get('list_products',[\App\Http\Controllers\Datatable\ProductController::class,'getAll']);
    Route::get('list_vehicles',[\App\Http\Controllers\Datatable\VehicleController::class,'getAll']);

    Route::get('list_sale_orders',[\App\Http\Controllers\Datatable\SaleOrderController::class,'getAll']);

});
