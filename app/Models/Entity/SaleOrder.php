<?php

namespace App\Models\Entity;

use App\Models\AppModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SaleOrder extends AppModel
{
    protected $table    =   'sale_orders';


    protected $fillable =   [
        'number',
        'vehicle_id',
        'date',
        'user_id'
    ];
}
