<?php

namespace App\Models\Entity;

use App\Models\AppModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends AppModel
{
    protected $table    =   'vehicles';

    protected $fillable =   [
        'code',
        'name'
    ];
}
