<?php

namespace App\Models\Entity;

use App\Models\AppModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends AppModel
{
    protected $table    =   'products';

    protected $fillable =   [
        'code',
        'name',
        'qty'
    ];
}
