<?php

namespace App\Models\Entity;

use App\Models\AppModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SaleOrderDetail extends AppModel
{
    protected $table    =   'sale_order_details';

    protected $fillable =   [
        'sale_order_id',
        'product_id',
        'qty'
    ];
}
