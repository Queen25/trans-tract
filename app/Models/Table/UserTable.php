<?php

namespace App\Models\Table;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserTable extends \App\Models\Entity\User
{
    use HasFactory;
}
