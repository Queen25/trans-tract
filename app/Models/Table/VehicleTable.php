<?php

namespace App\Models\Table;

use App\Models\Entity\Vehicle;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VehicleTable extends Vehicle
{
    use HasFactory;
}
