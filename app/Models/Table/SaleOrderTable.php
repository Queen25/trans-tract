<?php

namespace App\Models\Table;

use App\Models\Entity\SaleOrder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SaleOrderTable extends SaleOrder
{
    public function order_items()
    {
        return $this->hasMany(SaleOrderDetailTable::class, 'sale_order_id');
    }

    public function vehicles()
    {
        return $this->belongsTo(VehicleTable::class,'vehicle_id');
    }

}
