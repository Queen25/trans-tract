<?php

namespace App\Models\Table;

use App\Models\Entity\SaleOrderDetail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SaleOrderDetailTable extends SaleOrderDetail
{
    use HasFactory;
}
