<?php

namespace App\Http\Requests\Product;

use App\Http\Requests\InitialRequestValidation;
use Illuminate\Foundation\Http\FormRequest;

class CreateVechicleRequest extends FormRequest
{
    use InitialRequestValidation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'      =>  'required|unique:vehicles,code',
            'name'      =>  'required|string',
        ];
    }

    public function messages()
    {
        return [
            'name.required' =>  trans('validation.required', ['attribute' => 'Name']),
            'code.required' =>  trans('validation.required', ['attribute' => 'Code']),
            'code.unique' =>  trans('validation.unique', ['attribute' => 'Code']),
        ];
    }
}
