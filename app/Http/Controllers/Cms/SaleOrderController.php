<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Transaction\CreateSaleOrderRequest;
use App\Services\Product\ProductService;
use App\Services\SaleOrder\SaleOrderService;
use App\Services\Vehicle\VehicleService;
use Illuminate\Http\Request;

class SaleOrderController extends ApiController
{
    protected $saleOrderService;
    protected $vehicleService;
    protected $productService;

    public function __construct(
        ProductService $productService,
        VehicleService $vehicleService,
        SaleOrderService $saleOrderService,
        Request $request)
    {
        parent::__construct($request);
        $this->saleOrderService  =   $saleOrderService;
        $this->vehicleService   =   $vehicleService;
        $this->productService   =   $productService;
    }

    public function index()
    {
        return view("sale_order.index");
    }

    public function create()
    {
        return view("sale_order.create");
    }

    public function findVehicle(Request $request): \Illuminate\Http\JsonResponse
    {
        $input  =   $request->all();
        $result =   $this->vehicleService->getByCode($input);

        try {
            if ($result->success) {
                $response = $result->data;
                return $this->sendSuccess($response, $result->message, $result->code);
            }

            return $this->sendError($result->data, $result->message, $result->code);
        } catch (\Exception $exception) {
            return $this->sendError($exception->getMessage(),"",500);
        }
    }

    public function showProduct(Request $request): \Illuminate\Http\JsonResponse
    {
        $input  =   $request->all();
        return $this->productService->loadProduct($input);
    }

    public function addProduct(Request $request): \Illuminate\Http\JsonResponse
    {
        $input  =   $request->all();
        return $this->productService->addProduct($input);
    }

    public function store(CreateSaleOrderRequest $request)
    {
        $input  =   $request->all();
        $result =   $this->saleOrderService->create($input);

        try {
            if ($result->success) {
                $response = $result->data;
                return $this->sendSuccess($response, $result->message, $result->code);
            }

            return $this->sendError($result->data, $result->message, $result->code);
        } catch (\Exception $exception) {
            return $this->sendError($exception->getMessage(),"",500);
        }
    }
}
