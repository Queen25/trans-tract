<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Product\CreateVechicleRequest;
use App\Http\Requests\Product\UpdateVechicleRequest;
use App\Services\Vehicle\VehicleService;
use Illuminate\Http\Request;

class VehicleController extends ApiController
{
    protected $vehicleService;

    public function __construct(
        VehicleService $vehicleService,
        Request $request)
    {
        parent::__construct($request);
        $this->vehicleService  =   $vehicleService;
    }

    public function index()
    {
        return view("vehicles.index");
    }

    public function create()
    {
        return view("vehicles.create");
    }

    public function edit($id)
    {
        $data   =   $this->vehicleService->getById($id);
        return view("vehicles.edit",['data' => $data]);
    }

    public function store(CreateVechicleRequest $request)
    {
        $input  =   $request->all();
        $result =   $this->vehicleService->create($input);

        try {
            if ($result->success) {
                $response = $result->data;
                return $this->sendSuccess($response, $result->message, $result->code);
            }

            return $this->sendError($result->data, $result->message, $result->code);
        } catch (\Exception $exception) {
            return $this->sendError($exception->getMessage(),"",500);
        }
    }

    public function update(UpdateVechicleRequest $request)
    {
        $input  =   $request->all();
        $result =   $this->vehicleService->update($input['id'],$input);

        try {
            if ($result->success) {
                $response = $result->data;
                return $this->sendSuccess($response, $result->message, $result->code);
            }

            return $this->sendError($result->data, $result->message, $result->code);
        } catch (\Exception $exception) {
            return $this->sendError($exception->getMessage(),"",500);
        }
    }

    public function destroy($id)
    {
        $result = $this->vehicleService->delete($id);

        try {
            if ($result->success) {
                $response = $result->data;
                return $this->sendSuccess($response, $result->message, $result->code);
            }

            return $this->sendError($result->data, $result->message, $result->code);
        } catch (\Exception $exception) {
            return $this->sendError($exception->getMessage(),"",500);
        }
    }
}
