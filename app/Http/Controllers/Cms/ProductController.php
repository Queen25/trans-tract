<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Product\CreateProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Services\Product\ProductService;
use Illuminate\Http\Request;

class ProductController extends ApiController
{
    protected $productService;

    public function __construct(
        ProductService $productService,
        Request $request)
    {
        parent::__construct($request);
        $this->productService  =   $productService;
    }

    public function index()
    {
        return view("products.index");
    }

    public function create()
    {
        return view("products.create");
    }

    public function edit($id)
    {
        $data   =   $this->productService->getById($id);
        return view("products.edit",['data' => $data]);
    }

    public function store(CreateProductRequest $request)
    {
        $input  =   $request->all();
        $result =   $this->productService->create($input);

        try {
            if ($result->success) {
                $response = $result->data;
                return $this->sendSuccess($response, $result->message, $result->code);
            }

            return $this->sendError($result->data, $result->message, $result->code);
        } catch (\Exception $exception) {
            return $this->sendError($exception->getMessage(),"",500);
        }
    }

    public function update(UpdateProductRequest $request)
    {
        $input  =   $request->all();
        $result =   $this->productService->update($input['id'],$input);

        try {
            if ($result->success) {
                $response = $result->data;
                return $this->sendSuccess($response, $result->message, $result->code);
            }

            return $this->sendError($result->data, $result->message, $result->code);
        } catch (\Exception $exception) {
            return $this->sendError($exception->getMessage(),"",500);
        }
    }

    public function destroy($id)
    {
        $result = $this->productService->delete($id);

        try {
            if ($result->success) {
                $response = $result->data;
                return $this->sendSuccess($response, $result->message, $result->code);
            }

            return $this->sendError($result->data, $result->message, $result->code);
        } catch (\Exception $exception) {
            return $this->sendError($exception->getMessage(),"",500);
        }
    }
}
