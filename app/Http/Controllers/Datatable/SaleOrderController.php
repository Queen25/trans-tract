<?php

namespace App\Http\Controllers\Datatable;

use App\Http\Controllers\Controller;
use App\Services\SaleOrder\SaleOrderService;
use Illuminate\Http\Request;

class SaleOrderController extends Controller
{
    protected $saleOrderService;

    /**
     * @param SaleOrderService $saleOrderService
     */
    public function __construct(SaleOrderService $saleOrderService)
    {
        $this->saleOrderService = $saleOrderService;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll()
    {
        return $this->saleOrderService->getAll();
    }
}
