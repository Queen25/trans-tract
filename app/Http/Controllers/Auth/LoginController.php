<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Models\Table\UserTable;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username(): string
    {
        return 'username';
    }

    protected function validateLogin(Request $request): void
    {
        $customMessages = [
            'username.required' => trans('validation.required',['name' => 'Username']),
            'password.required' => trans('validation.required',['name' => 'Password'])
        ];

        $this->validate($request,[
            $this->username() => 'required|string',
            'password'  => 'required|string'
        ],$customMessages);
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        $user = UserTable::query()->where('username',$request->username)->first();

//        if( $user && !$user->status){
//            return redirect()->back()->withErrors([
//                'info' => 'Your account is inactive. Please contact the Administrator'
//            ]);
//        }

        if (empty($user)) {
            return redirect()->back()->withErrors([
                'info' => 'Account not registered'
            ]);
        }

        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function sendFailedLoginResponse(Request $request): void
    {
        throw ValidationException::withMessages([
            'info' => ['Account not registered'],
        ]);
    }
}
