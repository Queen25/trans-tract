<?php

namespace App\Services\SaleOrder;

use App\Models\AppModel;
use App\Models\Table\ProductTable;
use App\Models\Table\SaleOrderDetailTable;
use App\Models\Table\SaleOrderTable;
use App\Models\Table\VehicleTable;
use App\Services\AppService;
use App\Services\AppServiceInterface;
use App\Services\Vehicle\VehicleService;
use Illuminate\Database\Eloquent\Model;

class SaleOrderService extends AppService implements AppServiceInterface
{
    protected $vehicleTable;
    protected $productTable;

    public function __construct(
        VehicleTable $vehicleTable,
        ProductTable $productTable,
        SaleOrderTable $model)
    {
        $this->vehicleTable   =   $vehicleTable;
        $this->productTable     =   $productTable;
        parent::__construct($model);
    }

    public function getAll()
    {
        $model = $this->model->query()->with('vehicles')->orderBy('created_at','DESC');

        return \DataTables::eloquent($model)->addIndexColumn()->toJson();

    }

    public function getPaginated($search = null, $perPage = 15)
    {
        // TODO: Implement getPaginated() method.
    }

    public function getById($id)
    {
        $result =   $this->model->newQuery()->find($id);

        return $this->sendSuccess($result);
    }

    function randomString($length = null)
    {
        $str = "";
        $characters = array_merge(range('0','9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }

    public function create($data)
    {
        $data['apps']['date']  = date('Y-m-d');
        \DB::beginTransaction();
        try {
            $reqOrder  = $data['order'];

            $vehicle  = $this->vehicleTable->newQuery()->where('code',$reqOrder['vehiclesId'])->first();

            $order                      = new $this->model($reqOrder);
            $order->user_id             = auth()->user()->id;
            $order->date                = date('Y-m-d');
            $order->number              =  "SO-"."TRX"."-".$this->randomString(6);
            $order->vehicle_id          = $vehicle->id;


            if ($order->save()) {
                $items = $data['items'];
                if (isset($items['product_id'])) {
                    foreach ($items['product_id'] as $key=>$item) {
                        $order->order_items()->save(new SaleOrderDetailTable([
                            "product_id" => $item,
                            "qty" => $items['qty'][$key]
                        ]));

                        $productId = $this->productTable->newQuery()->where([
                            'id'=> $item
                        ])->first();
                        if ($productId) {
                            $productId->update(['qty'=>($productId->qty - $items['qty'][$key])]);
                        }
                    }
                }
            }

            \DB::commit(); // commit the changes
            return $this->sendSuccess($order);
        } catch (\Exception $exception) {
            \DB::rollBack(); // rollback the changes
            return $this->sendError(null, $this->debug ? $exception->getMessage() : null);
        }
    }

    public function update($id, $data)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }
}
