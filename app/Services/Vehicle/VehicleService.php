<?php

namespace App\Services\Vehicle;

use App\Models\Table\VehicleTable;
use App\Services\AppService;
use App\Services\AppServiceInterface;

class VehicleService extends AppService implements AppServiceInterface
{
    public function __construct(VehicleTable $model)
    {
        parent::__construct($model);
    }

    public function getAll()
    {
        $model = $this->model->query()->orderBy('created_at','DESC');

        return \DataTables::eloquent($model)->addIndexColumn()->toJson();
    }

    public function getPaginated($search = null, $perPage = 15)
    {
        // TODO: Implement getPaginated() method.
    }

    public function getById($id)
    {
        $result =   $this->model->newQuery()->find($id);
        return $this->sendSuccess($result);
    }

    public function getByCode($code)
    {
        $result =   $this->model->newQuery()->where('code',$code)->first();

        return $this->sendSuccess($result);
    }


    public function create($data)
    {
        \DB::beginTransaction();

        try {

            $div    =   $this->model->newQuery()->create([
                'code'      =>  $data['code'],
                'name'      =>  $data['name']
            ]);

            \DB::commit(); // commit the changes
            return $this->sendSuccess($div);
        } catch (\Exception $exception) {
            \DB::rollBack(); // rollback the changes
            return $this->sendError(null, $this->debug ? $exception->getMessage() : null);
        }
    }

    public function update($id, $data)
    {
        $read   =   $this->model->newQuery()->find($id);
        \DB::beginTransaction();

        try {

            $read->code     =   $data['code'];
            $read->name     =   $data['name'];

            $read->save();

            \DB::commit(); // commit the changes
            return $this->sendSuccess($read);
        } catch (\Exception $exception) {
            \DB::rollBack(); // rollback the changes
            return $this->sendError(null, $this->debug ? $exception->getMessage() : null);
        }
    }

    public function delete($id)
    {
        $div   =   $this->model->newQuery()->find($id);
        try {

            $div->delete();

            \DB::commit(); // commit the changes
            return $this->sendSuccess($div);
        } catch (\Exception $exception) {
            \DB::rollBack(); // rollback the changes
            return $this->sendError(null, $this->debug ? $exception->getMessage() : null);
        }
    }
}
