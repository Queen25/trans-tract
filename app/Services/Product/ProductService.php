<?php

namespace App\Services\Product;

use App\Models\Table\ProductTable;
use App\Services\AppService;
use App\Services\AppServiceInterface;
use Illuminate\Database\Eloquent\Model;

class ProductService extends AppService implements AppServiceInterface
{
    public function __construct(ProductTable $model)
    {
        parent::__construct($model);
    }

    public function getAll()
    {
        $model = $this->model->query()->orderBy('created_at','DESC');

        return \DataTables::eloquent($model)->addIndexColumn()->toJson();
    }

    public function getPaginated($search = null, $perPage = 15)
    {
        // TODO: Implement getPaginated() method.
    }

    public function getById($id)
    {
        $result =   $this->model->newQuery()->find($id);

        return $this->sendSuccess($result);
    }

    public function create($data)
    {
        \DB::beginTransaction();

        try {

            $div    =   $this->model->newQuery()->create([
                'code'      =>  $data['code'],
                'name'      =>  $data['name'],
                'qty'       =>  $data['qty']
            ]);

            \DB::commit(); // commit the changes
            return $this->sendSuccess($div);
        } catch (\Exception $exception) {
            \DB::rollBack(); // rollback the changes
            return $this->sendError(null, $this->debug ? $exception->getMessage() : null);
        }
    }

    public function update($id, $data)
    {
        $read   =   $this->model->newQuery()->find($id);
        \DB::beginTransaction();

        try {

            $read->code     =   $data['code'];
            $read->name     =   $data['name'];
            $read->qty      =   $data['qty'];

            $read->save();

            \DB::commit(); // commit the changes
            return $this->sendSuccess($read);
        } catch (\Exception $exception) {
            \DB::rollBack(); // rollback the changes
            return $this->sendError(null, $this->debug ? $exception->getMessage() : null);
        }
    }

    public function delete($id)
    {
        $div   =   $this->model->newQuery()->find($id);

        try {

            $div->delete();

            \DB::commit(); // commit the changes
            return $this->sendSuccess($div);
        } catch (\Exception $exception) {
            \DB::rollBack(); // rollback the changes
            return $this->sendError(null, $this->debug ? $exception->getMessage() : null);
        }
    }


    public function loadProduct($data): \Illuminate\Http\JsonResponse
    {
        $product = $this->model->newQuery()
            ->whereNotIn('id', $data['product_id'])
            ->where('qty','>',0)
            ->orderBy('name','ASC')
            ->get();

        $html = view("sale_order.inc.listProduct", compact('product'))->renderSections();
        if ($product) {
            $html = $html;
        } else {
            $html = null;
        }
        return response()->json(['html' => $html]);
    }

    public function addProduct($data)
    {
        $product = $this->model->newQuery()->whereIn('id', $data['product_id'])->get();

        $html = view("sale_order.inc.appendProduct", compact('product'))->renderSections();
        if ($product) {
            $html = $html;
        } else {
            $html = null;
        }
        return response()->json(['html' => $html]);
    }
}
