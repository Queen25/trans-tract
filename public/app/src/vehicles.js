'use strict'

const {createData, deleteData} = require("../api");

var DataProducts = function () {
    var initTable1 = function () {
        var table = $('#kt_datatable_vehicles');

        // begin first table
        table.DataTable({
            "responsive": true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax: '/list_vehicles',
            columns: [
                { data: 'code' },
                { data: 'name' },
                {},
            ],
            columnDefs: [
                {
                    targets: -1,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        return `
                        <a href="/vehicles/` + full.id + `/edit" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit">
                          <i class="fa fa-pencil-alt"></i>
                        </a>
                        <a data=` + full.id + ` href="#" onclick="deleteData(this,event)" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete">
                          <i class="fa fa-trash"></i>
                        </a>`;
                    },
                },
            ],
        });
    };
    return {
        //main function to initiate the module
        init: function () {
            initTable1();
        },
    };
}();

jQuery(document).ready(function () {
    DataProducts.init();
});

window.createData = (input, evt) => {
    evt.preventDefault();

    const data = {
        'code' : getValue('code'),
        'name' : getValue('name')
    }

    beforeLoadingAttr('#saveBtn')
    Swal.fire({
        title: 'Save Confirmation',
        text: "Are you sure you save this data",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Save',
        cancelButtonText: 'Back',
    }).then((result) => {
        if (result.isConfirmed) {
            createData('/vehicles', data).then(res => {
                let response = res.data
                if (response.success) {
                    afterLoadingAttr('#saveBtn')
                    messages('Data saved successfully', '/vehicles')
                }
            }).catch(err => {
                afterLoadingAttr('#saveBtn')
                let error = err.response.data
                console.log(error)
                if (!error.success) {
                    toastr.error(error.message)
                }
            })
        }
    })


}

window.updateData = (input, evt) => {
    evt.preventDefault();

    const data = {
        'id' : getValue('id'),
        'code' : getValue('code'),
        'name' : getValue('name')
    }

    beforeLoadingAttr('#saveBtn')
    Swal.fire({
        title: 'Save Confirmation',
        text: "Are you sure you update this data",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Save',
        cancelButtonText: 'Back',
    }).then((result) => {
        if (result.isConfirmed) {
            createData('/updateVehicles', data).then(res => {
                let response = res.data
                if (response.success) {
                    afterLoadingAttr('#saveBtn')
                    messages('Data saved successfully', '/vehicles')
                }
            }).catch(err => {
                afterLoadingAttr('#saveBtn')
                let error = err.response.data
                console.log(error)
                if (!error.success) {
                    toastr.error(error.message)
                }
            })
        }
    })


}

window.deleteData = input => {
    var data = {id: $(input).attr('data')};
    Swal.fire({
        title: 'Warning',
        text: "Are you sure deleting this data?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            deleteData('/vehicles/' + $(input).attr('data')).then(res => {
                Swal.fire('Success!', res.data.message, 'success');
                window.location.reload();
            })
        }
    })
}
