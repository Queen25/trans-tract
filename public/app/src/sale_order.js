'use strict'

const {createData, deleteData, getResult} = require("../api");

var DataSalesOrder = function () {
    var initTable1 = function () {
        var table = $('#kt_datatable_sale_orders');

        // begin first table
        table.DataTable({
            "responsive": true,
            searchDelay: 500,
            processing: true,
            ajax: '/list_sale_orders',
            columns: [
                { data: 'number' },
                { data: 'date' },
                { data : 'vehicles.name'},
                {},
            ],
            columnDefs: [
                {
                    targets: -1,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        return `
                        <a href="/products/` + full.id + `" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit">
                          <i class="fa fa-eye"></i>
                        </a>`;
                    },
                },
            ],
        });
    };
    return {
        //main function to initiate the module
        init: function () {
            initTable1();
        },
    };
}();

jQuery(document).ready(function () {
    DataSalesOrder.init();
});

window.findVehicle = (evt) => {
    evt.preventDefault();
    let phone = $('#phone_customer').val();
    beforeLoadingAttr('#BtnSave')
    getResult('/findVehicle',{id: phone}).then(res => {
        let response = res.data
        if (response.success) {
            if (response.data === null) {
                toastr.error("Vehicle Not Found")
            }
            $('#name').val(response.data.name)
            afterLoadingAttr('#BtnSave')
        } else {

        }
    })
}

window.showAddProduct = (input,evt) => {
    evt.preventDefault();

    let products = $('.product-list');
    let product = [];

    for (let i = 0; i < products.length; i++) {
        product.push($(products[i]).attr('data-id'));
    }

    if (product.length <= 0) {
        product = [0];
    }

    beforeLoadingAttr('#btnProduct')
    getResult('/showProduct',{product_id : product}).then(res => {
        let response = res.data
        if (response.html.content === "") {
            Swal.fire({
                icon: 'warning',
                title: 'Oops...',
                text: 'Data Product Not Found',
            })
        } else {
            afterLoadingAttr('#btnProduct')
            $('.data-detail').html(response.html.content);
            $('.product-po').dataTable({
                columnDefs: [
                    { "searchable": false, "targets": 0 },
                    { "orderable": false, "targets": 0 },
                ]
            });
            $('.bs-modal-lg').modal('show');
        }
    }).catch(err => {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Failed To Load Product',
        })
    })
}

window.getCheckedBoxesSellout = (checkBoxName) => {
    let checkboxes = $('.' + checkBoxName);
    let checkboxesChecked = [];

    for (let i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked) {
            checkboxesChecked.push($(checkboxes[i]).attr('data-id'));
        }
    }

    if (checkboxesChecked.length > 0) {
        getResult('/addProduct', {product_id:checkboxesChecked}).then(res => {
            $('#productItems').append(res.data.html.content);
            $('.bs-modal-lg').modal('hide');
        }).catch(err => {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Failed To Load Product',
            })
        })
    }

    return checkboxesChecked.length > 0 ? checkboxesChecked : null;
}

window.removeData = (input,evt) => {
    $('tr#product_' + $(input).attr('data-product')).remove();
    evt.preventDefault();
}

window.createSellout = (input, evt) => {
    evt.preventDefault();

    // product
    var products = $('.product-list');
    var product = [];

    // trade

    var quantity = [];

    var vehiclesId = document.getElementById('vehicle').value;

    for (var i = 0; i < products.length; i++) {
        product.push($(products[i]).attr('data-id'));
        quantity.push($($('.quantity')[i]).val());
    }

    var sellout = {
        order: {
            vehiclesId: vehiclesId,
        },
        items: {
            product_id: product,
            qty: quantity
        },
    };

    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Submit it!'
    }).then((result) => {
        if (result.value) {
            createData('/sales_orders',sellout).then(res => {
                let response = res.data
                if (response.success) {
                    messages('Success','/sales_orders')
                }
            }).catch(err => {
                let error = err.response.data
                console.log(error)
                if(!error.success) {
                    toastr.error(error.message)
                }
            })
        }
    })
}

