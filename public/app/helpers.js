'use strict'

window.beforeLoadingAttr = (el) => {
    $(el).addClass("btn btn-brand kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light");
}

window.afterLoadingAttr = (el) => {
    $(el).removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light");
}

window.isNumber = evt => {
    let charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}

window.getValue = (element) => {
    let el = document.getElementById(element)
    if (el != null) {
        return el.value
    } else {
        return null
    }
}

window.getRadioValue = (element) => {
    let radios = document.getElementsByName(element);
    for (let i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
            return radios[i].value;
            break;
        }
    }
}


window.messages = (message,url) => {
    Swal.fire({
        title: 'Success',
        text: message,
        icon: 'success',
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'OK',
        allowOutsideClick: false
    }).then((result) => {
        if (result.value) {
            window.location.href = url;
        }
    })
}

export default messages
