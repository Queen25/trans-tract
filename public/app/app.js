'use strict'

window.Swal         = require('sweetalert2');
window.axios        = require('axios');
window.toastr       = require('toastr');
window.select2      = require('select2');
window.dayJs        =   require('dayjs')

import 'dayjs/locale/id'
dayJs.locale('id')

import 'sweetalert2/src/sweetalert2.scss'

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

