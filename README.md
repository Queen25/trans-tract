# Test Trans Track


## Installation

Use the package manager [composer](https://getcomposer.org/) to install Test Trans Track.

```bash
composer install
```

```bash
yarn or npm install
yarn or npm run dev
```

## Usage

```php
cp .env.example .env

# returns 'Key'
php artisan key:generate

# returns 'Migrate Table'
php artisan migrate

# returns 'Seed Table'
php artisan db:seed --class=UserTableSeeder

# returns 'URL Return'
php artisan serve
```
